# Bookstore

[![Build Status](https://travis-ci.org/tecris/bookstore.svg?branch=master)](https://travis-ci.org/tecris/bookstore)

 - JEE & Continuous Delivery playground
 - Project generated with JBoss Forge (thank you forge :) )

| *Technology*  | *Version* |
| ------------- | ------------- |
| Java | 8 |
| Wildfly | 10.0.0 |
| Docker | 1.10 |
| Docker Compose | 1.6 |
| Maven | 3.3 |

## Simple Continuous Delivery
 * One liner   
   * `$ mvn verify -Pcontinuous-delivery -Dmaven.buildNumber.doCheck=false`
 * Steps performed
   * build artifact
   * start empty mysql database docker container
   * deploy database schema with [flyway](http://flywaydb.org)
   * build wildfly docker image with latest artifact
   * start wildfly container
   * run integration tests
   * stop and remove mysql database container
   * stop and remove wildfly container

## Basic E2E with Docker & Docker Compose

  ```
  $ docker-compose up -d                # fire-up db and web containers
  $ mvn clean compile flyway:migrate    # deploy database scripts
  $ mvn clean wildfly:deploy            # deploy application`
  $ mvn clean integration-test          # run E2E tests
 ```

## Create tag
 - `$ mvn -Darguments="-DskipITs" release:prepare`
